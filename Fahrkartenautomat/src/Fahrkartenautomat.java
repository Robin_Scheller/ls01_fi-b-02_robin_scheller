﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
   int count = 0; 	
   boolean turnOFF;
    
   do{
	 turnOFF = durchlauf();
   }while(turnOFF == false);
        
    }
    public static void rueckgeldAusgeben(double zuZahlenderBetrag,double eingezahlterGesamtbetrag){
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
         if(rückgabebetrag > 0.0)
         {
      	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
      	   System.out.println("wird in folgenden Münzen ausgezahlt:");

             while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
             {
          	  System.out.println("2 EURO");
  	          rückgabebetrag -= 2.0;
             }
             while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
             {
          	  System.out.println("1 EURO");
  	          rückgabebetrag -= 1.0;
             }
             while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
             {
          	  System.out.println("50 CENT");
  	          rückgabebetrag -= 0.5;
             }
             while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
             {
          	  System.out.println("20 CENT");
   	          rückgabebetrag -= 0.2;
             }
             while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
             {
          	  System.out.println("10 CENT");
  	          rückgabebetrag -= 0.1;
             }
             while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
             {
          	  System.out.println("5 CENT");
   	          rückgabebetrag -= 0.05;
             }
         }
         
         
         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            "vor Fahrtantritt entwerten zu lassen!\n"+
                            "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static void fahrkartenAusgabe() {	
    	System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");
    }
    
	public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
		
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		
		eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("%s%.2f%s\n","Noch zu zahlen: " , + (zuZahlenderBetrag - eingezahlterGesamtbetrag),"Euro");
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
	       return eingezahlterGesamtbetrag;
	}

	public static double fahrkartenbestellungErfassen(Scanner tastatur	) {
		
		double zuZahlenderBetrag = 0.00;
		int fahrkartenWahl;
		boolean goOn = true;
		int count = 0;
		
		
		do {
			System.out.printf("%s%n%n%s%n%n%s%n%n%s%n%n%s%n%n","Wählen sie eine Fahrkarte:","Einzelfahrschein Regeltarif 2,90€ [1]","Tagesfahrschein Regeltarif 8,50€ [2]","Gruppenfahrschein Regeltarif 23,90€ [3]","Weiter zur Bezahlung [4]");
			fahrkartenWahl = tastatur.nextInt();
			switch(fahrkartenWahl){
				case 1:
					zuZahlenderBetrag = zuZahlenderBetrag + 2.90;
					count = count +1;
					break;
				case 2:	
					zuZahlenderBetrag = zuZahlenderBetrag + 8.50;
					count = count +1;
					break;
				case 3:	
					zuZahlenderBetrag = zuZahlenderBetrag + 23.90;
					count = count +1;
					break;	
				case 4:	
					goOn = false;
					break;	
			}
		}while(count < 10 & goOn == true);
	       
	       
	       
	       return zuZahlenderBetrag;
	}
	
	public static boolean durchlauf() {
		
		boolean turnOFF;
		
		Scanner tastatur = new Scanner(System.in);
	     
	     double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur); 
	       
	     double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag,tastatur);
	      
	      fahrkartenAusgabe(); 
	      
	      rueckgeldAusgeben(zuZahlenderBetrag,eingezahlterGesamtbetrag);
	      
	      System.out.print("Wollen sie das system beenden? y/n ");
	       char systemTurnOFF = tastatur.next().charAt(0);
	       
	       if(systemTurnOFF == 'y'||systemTurnOFF == 'Y') {
	    	   turnOFF = true;
	       }else {
	    	   turnOFF = false;
	       }
	     
	      return turnOFF;
	}
}